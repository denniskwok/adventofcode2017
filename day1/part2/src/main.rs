use std::fs::File;
use std::io::prelude::*;

fn main() {
	let mut f = File::open("input.txt").expect("file not found");

	let mut contents = String::new();
	f.read_to_string(&mut contents)
		.expect("something went wrong reading this file");

	let mut as_ints: Vec<u64> = Vec::new();
	let mut iter = contents.lines();

	for line in iter {
		solve(line);
	}

}

fn solve(input_string: &str) {
	let mut as_ints: Vec<u64> = Vec::new();
	let mut matches: Vec<u64> = Vec::new();

	for c in input_string.chars() {
		let the_number = c.to_string().parse::<u64>();
		let the_number = match the_number {
			Ok(u64) => u64,
			Err(_error) => {
				continue;
			},
		};

		as_ints.push(the_number);
		// println!("{}", the_number);
	}

	let num_ints = as_ints.len();

	for i in 0..as_ints.len() {
		let compare_index = (num_ints / 2 + i) % num_ints;
		if as_ints[i] == as_ints[compare_index] {
			matches.push(as_ints[i]);
		}
	}


	let mut sum = 0;
	for i in matches.iter() {
		sum += i;
	}

	println!("{}", sum)
}

