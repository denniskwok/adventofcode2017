use std::fs::File;
use std::io::prelude::*; // weird to me that read_to_string is in here

fn main() {
	let mut f = File::open("input.txt").expect("file not found");

	let mut contents = String::new();
	f.read_to_string(&mut contents)
		.expect("IO error of some kind");

    let iter = contents.lines();
    let mut sum = 0;

    for line in iter {
    	sum += solve_line(line);
    }

    println!("{}", sum)
}

fn solve_line(input_string: &str) -> i32 {
	let mut smallest = 99999999;
	let mut largest = -1;

	let split = input_string.split_whitespace();
	for i in split {
		let as_int = i.parse::<i32>();
		let as_int = match as_int {
			Ok(i32) => i32,
			Err(_error) => {
				continue;
			},
		};

		if smallest > as_int {
			smallest = as_int;
		}

		if largest < as_int {
			largest = as_int;
		}

		// println!("{}", as_int)
	}

	return largest - smallest
}
