use std::fs::File;
use std::io::prelude::*;

fn main() {
    let mut f = File::open("input.txt").expect("file not found");

	let mut contents = String::new();
	f.read_to_string(&mut contents)
		.expect("IO error of some kind");

    let iter = contents.lines();
    let mut sum = 0;

    for line in iter {
    	sum += solve_line(line);
    }

    println!("{}", sum)
}

fn solve_line(input_string: &str) -> i32 {
	let split = input_string.split_whitespace();
	let mut numbers: Vec<i32> = Vec::new();

	for i in split {
		let as_int = i.parse::<i32>();
		let as_int = match as_int {
			Ok(i32) => i32,
			Err(_error) => {
				continue;
			},
		};

		numbers.push(as_int);
	}

	numbers.sort();

	// bigger numbers can only be divisible by smaller numbers
	for i in 0..numbers.len() {
		let large_number_index = numbers.len() - i - 1;
		let large_number = numbers[large_number_index];

		for j in 0..numbers.len() {
			// both ends of the array meet themselves, no matches
			if large_number_index == j {
				break;
			}

			let small_number = numbers[j];

			// small number is more than half large number, no more matches
			if small_number * 2 > large_number {
				break;
			}

			if large_number % small_number == 0 {
				return large_number / small_number;
			}
		}
	}

	return 0;
}