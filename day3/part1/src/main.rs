
enum Directions {
	RIGHT,
	UP,
	LEFT,
	DOWN,
}

fn main() {
    let steps = 277678;

    let mut x = 0i32;
    let mut y = 0i32;
    let mut direction = Directions::RIGHT;
    let mut sub_step = 0;
    let mut sub_step_max = 1;

    for _ in 0..steps-1 {

    	match direction {
    		Directions::RIGHT => x += 1,
    		Directions::UP => y += 1,
    		Directions::LEFT => x -= 1,
    		Directions::DOWN => y -= 1,
    	}

    	sub_step += 1;

    	if sub_step == sub_step_max {
    		match direction {
    			Directions::RIGHT => {
    				direction = Directions::UP;
    			}
    			Directions::UP => {
    				direction = Directions::LEFT;
    				sub_step_max += 1;
    			}
    			Directions::LEFT => {
    				direction = Directions::DOWN;
    			}
    			Directions::DOWN => {
    				direction = Directions::RIGHT;
    				sub_step_max += 1;
    			}
    		}

    		sub_step = 0;
    	}

    	

    	println!("{} {}", x, y);
    }

    println!("{}", x.abs() + y.abs())
}

